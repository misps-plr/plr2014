<?php

	require '../src/Plr.php';
	require '../sso/functions.php';

	#
	$plr = new Plr();
	$sso = new SSO_Client();
	$sso->is_loggedin();

	# check if user is an adm to access this area!
	if($plr->is_adm() == false){
		$sso->redirect_to($sso->get_config('service_url','continue'));
	}

	#
	$part = explode(' ', $sso->get_user_data('full_name'));
	setcookie('jsnm', ucfirst($part[0]));
?>

<!doctype html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Inicio | Administração</title>
	<link href="./../favicon.png" rel="shortcut icon" />
	<link href="./../assets/css/plr2014.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./../assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.Widget.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.Modal.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.tabControl.min.js"></script>
	<script type="text/javascript" src="./../assets/js/plr2014.js"></script>
</head>
<body onload="">
	<div class="container">
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./../sso/logout?continue=<?php echo $sso->get_current_url();?>" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name" ><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./../"><img src="./../assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							
							<li class="fr">
								<a href="./reports" class="btn-nav-sup" title="Reports" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-wrench"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content">
			<div class="inner">
				<!-- O CONTEUDO VAI AQUEEE o/-->

				<div class="tab-control" data-role="tab-control">
					<ul class="tabs">
						<li class="active"><a href="#gr_status">Status</a></li>
					</ul>
					<div class="frames">
						<div class="frame" id="gr_status">
						</div>
					</div>
				</div>

				<!-- O CONTEUDO VAI AQUEEE o/-->
				<div class="clear"></div>
			</div>
		</div>

		<div class="footer">
			<div class="inner">© <?php echo (date('Y') == 2014) ? date('Y') : '2014 - '.date('Y') ;?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
	</div>

	<script type="text/javascript">
		window.onload = function(){
			everyTime();
		};
	</script>
</body>
</html>

