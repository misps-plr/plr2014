<?php

	#
	require '../src/Plr.php';
	require '../sso/functions.php';


	# instantiates classes
	$plr = new Plr();
	$sso  = new SSO_Client();


	# validates if the user is logged
	$sso->is_loggedin();

	# check if user is an adm to access this area!
	if($plr->is_adm() == false){
		$sso->redirect_to($sso->get_config('service_url','continue'));
	}
?>
<!doctype html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Resultados Reportados | Administração</title>
	<link href="./../favicon.png" rel="shortcut icon" />
	<link href="./../assets/css/plr2014.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./../assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.Widget.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.Modal.min.js"></script>
	<script type="text/javascript" src="./../assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./../assets/js/metro.tabControl.min.js"></script>
	<script type="text/javascript" src="./../assets/js/plr2014.js"></script>
</head>
<body>
	<div class="container">

		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout">
						<li class="fr logout"><a href="./../sso/logout.mis?continue=<?php echo $sso->get_current_url();?>" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					</li>
					<li class="fr user-meta">
						<span>Bem vindo(a), </span>
						<span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./../"><img src="./../assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							<li class="fr">
								<a href="./reports" class="btn-nav-sup" title="Reports" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-wrench"></i>
								</a>
							</li>
							<li class="fr">
								<a href="./index" class="btn-nav-sup" title="Administrar PLR" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');">
									<i class="icon-dashboard"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">

				<div class="tab-control" data-role="tab-control">
					<ul class="tabs">
						<li class="active"><a href="#rp_pend" onclick="saveTab('new')">Novos</a></li>
						<li class=""><a href="#rp_serv" onclick="saveTab('atend')">Em Atendimento</a></li>
						<li class=""><a href="#rp_solv" onclick="saveTab('solv')">Solucionados</a></li>
					</ul>
					<div class="frames">
						<div class="frame" id="rp_pend">
							<div class="metro-datatable">
								<table class="table striped bordered hovered dataTable" id="employees_new">
									<thead>
										<tr>
											<th class="uppercase" style="width: 70px;">id</th>
											<th class="uppercase" >agente</th>
											<th class="uppercase" >solicitante</th>
											<th class="uppercase" style="width: 130px;">data abertura</th>
											<th class="uppercase" style="width: 50px;"> </th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($plr->reports_novos() as $value){ ?>
										<tr>
											<td class="capitalized text-center"><?php echo strtolower($value[0]);?></td>
											<td class="capitalized text-left"><?php echo strtolower($value[4]);?> (<?php echo $value[3];?>)</td>
											<td class="capitalized text-center"><?php echo strtolower($value[2]);?> (<?php echo $value[1];?>)</td>
											<td class="capitalized text-center"><?php echo $plr->exibe_datahora($value[6]);?></td>
											<td class="capitalized text-center">
												<a href="#show" title="Tratar Report" data-report="<?php echo $value[0];?>"><i class="icon-cog"></i></a>
												<a href="#" title="Remover Report" data-report="<?php echo $value[0];?>" onclick="alomaua()"><i class="icon-remove"></i></a>
											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /end of pending -->

						<div class="frame" id="rp_serv">
							<div class="metro-datatable">
								<table class="table striped bordered hovered dataTable" id="employees_wip">
									<thead>
										<tr>
											<th class="uppercase" style="width: 70px;">id</th>
											<th class="uppercase" >agente</th>
											<th class="uppercase" >solicitante</th>
											<th class="uppercase" >analista</th>
											<th class="uppercase" style="width: 130px;">data abertura</th>
											<th class="uppercase" style="width: 50px;"> </th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($plr->reports_atendimento($sso->get_user_data('uid')) as $value){ ?>
										<tr style="vertical-align: middle;">
											<td class="capitalized text-center"><?php echo $value[0];?></td>
											<td class="capitalized text-left"><?php echo strtolower($value[4]);?> (<?php echo $value[3];?>)</td>
											<td class="capitalized text-center"><?php echo strtolower($value[2]);?> (<?php echo $value[1];?>)</td>
											<td class="capitalized text-center"><?php echo strtolower($value[6]);?></td>
											<td class="capitalized text-center"><?php echo $plr->exibe_datahora($value[7]);?></td>
											<td class="capitalized text-center">
												<a href="#show" title="Tratar Report" data-report="<?php echo $value[0];?>"><i class="icon-cog"></i></a>
												<a href="#" title="Remover Report" data-report="<?php echo $value[0];?>" onclick="alomaua()"><i class="icon-remove"></i></a>
											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /end of solved -->

						<div class="frame" id="rp_solv">
							<div class="metro-datatable">
								<table class="table striped bordered hovered dataTable" id="employees_solv">
									<thead>
										<tr>
											<th class="uppercase" style="width: 70px;">id</th>
											<th class="uppercase" >agente</th>
											<th class="uppercase" >solicitante</th>
											<th class="uppercase" >analista</th>
											<th class="uppercase" style="width: 130px;">data abertura</th>
											<th class="uppercase" style="width: 130px;">data solução</th>
											<!-- <th class="uppercase" style="width: 50px;"> </th>-->
										</tr>
									</thead>
									<tbody>
										<?php foreach($plr->reports_resolvidos($sso->get_user_data('uid')) as $value){ ?>
										<tr>
											<td class="capitalized text-center"><?php echo strtolower($value[0]);?></td>
											<td class="capitalized text-left"><?php echo strtolower($value[4]);?> (<?php echo $value[3];?>)</td>
											<td class="capitalized text-center"><?php echo strtolower($value[2]);?> (<?php echo $value[1];?>)</td>
											<td class="capitalized text-center"><?php echo strtolower($value[6]);?></td>
											<td class="capitalized text-center"><?php echo $plr->exibe_datahora($value[7]);?></td>
											<td class="capitalized text-center"><?php echo $plr->exibe_datahora($value[8]);?></td>
											<!-- <td class="capitalized text-center">
												<a href="#show" title="Tratar Report" data-report="<?php echo $value[0];?>"><i class="icon-cog"></i></a>
											</td>-->
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /end of solved -->

					</div>
				</div>
				<!-- /end tabs navigation -->


				<div class="clear"></div>
			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">© <?php echo (date('Y') == 2014) ? date('Y') : '2014 - '.date('Y') ;?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
		<!-- /end footer -->



		<div class="modal-container employee-data">
			<div class="modal-content"></div>
		</div>
		<!-- /end modal -->

		<div class="modal-container loading-spinner">
			<div id="floatingBarsG">
				<div class="blockG" id="rotateG_01"></div>
				<div class="blockG" id="rotateG_02"></div>
				<div class="blockG" id="rotateG_03"></div>
				<div class="blockG" id="rotateG_04"></div>
				<div class="blockG" id="rotateG_05"></div>
				<div class="blockG" id="rotateG_06"></div>
				<div class="blockG" id="rotateG_07"></div>
				<div class="blockG" id="rotateG_08"></div>
			</div>
		</div>
		<!-- /end modalspinner -->

		<div class="notify-container">
			<div class="notify shadow" style="background: transparent !important;" id="notifyid"></div>
		</div>
		<!-- /end notify container -->

	</div>

	<script type="text/javascript">
		//
		window.onload = function(){
			everyTime();
			//showTab();
		};

		// Jquery Feelings
		$(document).ready(function(){

			// DataTable
			init_dataTable('#employees_new', 3);
			init_dataTable('#employees_wip', 4);
			init_dataTable('#employees_solv', 5);

			// Modal
			$('#employees_new').on('click', 'a[href="#show"]', function(event){
				var report_id = $(this).data('report');

				$.ajax({
					crossDomain: 'true',
					type: 'GET',
					url: window.location.origin+'/plr2014/modal_report?vis=rpt1&rid='+report_id,
					beforeSend: function(){
						$('.loading-spinner').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							showClose: false
						});
					},
					success: function(result){
						$('.loading-spinner').hide();
		     			$('.modal-content').html(result); // escreve conteudo vindo via ajax
						$('.employee-data').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							keyboard: true,
							showClose: true
						});
		   			}
		   		});
			});


			$('#employees_wip').on('click', 'a[href="#show"]', function(event){
				var report_id = $(this).data('report');

				$.ajax({
					crossDomain: 'true',
					type: 'GET',
					url: window.location.origin+'/plr2014/modal_report?vis=rpt1&rid='+report_id,
					beforeSend: function(){
						$('.loading-spinner').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							showClose: false
						});
					},
					success: function(result){
						$('.loading-spinner').hide();
		     			$('.modal-content').html(result); // escreve conteudo vindo via ajax
						$('.employee-data').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							keyboard: true,
							showClose: true
						});
		   			}
		   		});
			});


		}); // ready function
	</script>
</body>
</html>