<?php

	require './src/functions.php';
	require './sso/functions.php';

	$main = new Main();
	$sso = new SSO_Client();

	$sso->is_loggedin();

	#
	$part = explode(' ', $sso->get_user_data('full_name'));
	setcookie('jsnm', ucfirst($part[0]));
?>

<!doctype html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Inicio | PLR 2014</title>
	<link href="./favicon.png" rel="shortcut icon" />
	<link href="./assets/css/plr2014.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.Modal.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./assets/js/plr2014.js"></script>
</head>
<body onload="">
	<div class="container">
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout?continue=<?php echo $sso->get_current_url();?>" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name" ><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							<li class="fr">
								<a href="./reportados" class="btn-nav-sup" title="Resultados Reportados" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-wrench"></i>
								</a>
							</li>
							<li class="fr">
								<a href="./equipe" class="btn-nav-sup" title="Resultados da Equipe" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-stats-up"></i>
								</a>
							</li>
							<?php if($sso->get_user_data('type') == 2){ ?>
							<li class="fr">
								<a href="./admin/index" class="btn-nav-sup" title="Administrar PLR" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');">
									<i class="icon-dashboard"></i>
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content">
			<div class="inner">
				<div class="user-info">
					<h1>Seus dados</h1>
					<h2><span>Gerente:</span> <?php echo ucwords($sso->get_user_data('manager_name'));?> | <span>Cargo:</span> <?php echo ucwords($sso->get_user_data('job_position'));?> | <span>Célula:</span> <?php echo ucwords($sso->get_user_data('work_cell'));?></h2>
				</div>
				<div class="separator"></div>
				
				<h1>Agentes por faixa de ganho</h1>
				<div id="comparativo" class="comparativo"></div>
				<div class="clear"></div>

				<div class="indicadores">
					<ul id="indicadores-seletor">
						<?php echo file_get_contents("http://172.23.14.155/plr2014/indicadoresPorGerente.mis?gerencia={$sso->get_user_data('manager')}");?>
					</ul>
				</div>

				<div class="clear"></div>
			</div>
		</div>

		<div class="footer">
			<div class="inner">© <?php echo (date('Y') == 2014) ? date('Y') : '2014 - '.date('Y') ;?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
	</div>

	<script type="text/javascript">
		window.onload = function(){
			agentesPorFaixa(<?php echo ($sso->get_user_data('manager') == '0') ? 9 :$sso->get_user_data('manager');?>);
			everyTime();
		};
	</script>
</body>
</html>