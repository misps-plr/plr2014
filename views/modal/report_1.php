<div class="reports">
	<div class="overview" style="display: block;">
		<div class="employee-name">
			<h1 class="f-left uppercase"><?php echo $report['agent_name']; ?></h1>
			<h3 class="f-left" style="margin-top: 11px; margin-left: 5px;">(<?php echo $report['agent_uid']; ?>)</h3>
			<div class="clear"></div>
		</div>
		<div class="problem-detail">
			<h2>Problema</h2>
			<?php echo $report['comment']; ?>
		</div>
		<div class="table-pomarola">
			<h2>Pomarola</h2>
			<?php echo ($crazyTable) ? $crazyTable : 'Não existem dados no pomarola do colaborador! ;(' ; ?>
			<div class="clear"></div>
		</div>
		<div class="metatada">
			<ul>
				<li class="f-left uppercase">Solicitado: <?php echo $plr->exibe_datahora($report['send_datetime']); ?></li>
				<li class="f-left uppercase">Por: <?php echo $report['sender_name']; ?></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="actions">
			<?php if($report['status'] != 'Solucionado'){ ?>
			<input type="button" onclick="tratar_report()" value="TRATAR" class="button fr success">
			<?php } ?>
		</div>

		<form action="">
			<input type="hidden" id="analyst_id" value="<?php echo $sso->get_user_data('uid'); ?>" />
			<input type="hidden" id="report_id" value="<?php echo $plr->report_id; ?>" />
		</form>

		<div class="clear"></div>
	</div>
</div>
<!-- FIM REPORT -->
