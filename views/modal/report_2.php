<div class="reports">
	<div class="overview" style="display: block;">
		<div class="employee-name">
			<h1 class="f-left uppercase"><?php echo $report['agent_name']; ?></h1>
			<h3 class="f-left" style="margin-top: 11px; margin-left: 5px;">(<?php echo $report['agent_uid']; ?>)</h3>
			<div class="clear"></div>
		</div>
		<div class="problem-detail">
			<?php echo $report['comment']; ?>
		</div>
		<form method="post">
			<div class="solution-detail">
				<h2>Solução:</h2>
				<textarea class="comment-area" id="comment" autofocus required <?php if($report['status'] == 'Solucionado'){ echo 'disabled'; };?> ><?php echo "{$plr->primeiroNome(ucfirst($report['sender_name']))}, "; echo strtolower($plr->saudacao());?>
					<?php echo $plr->assinatura();?>
				</textarea>
				<input type="hidden" id="analyst_id" value="<?php echo $sso->get_user_data('uid'); ?>" />
				<input type="hidden" id="report_id" value="<?php echo $plr->report_id; ?>" />
			</div>
		</form>
		<div class="metatada">
			<ul>
				<li class="f-left uppercase">Solicitado: <?php echo $plr->exibe_datahora($report['send_datetime']); ?></li>
				<li class="f-left uppercase">Por: <?php echo $report['sender_name']; ?></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="actions">
			<?php if($report['status'] != 'Solucionado'){ ?>
			<input type="button" onclick="responder_report()" id="TRATAMENTO_REPORT_F2" value="RESPONDER" class="button fr success">
			<?php } ?>
		</div>

		<div class="clear"></div>
	</div>
</div>
<!-- FIM REPORT -->



		