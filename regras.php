<?php

	require './src/functions.php';
	require './sso/functions.php';

	$main = new Main();
	$sso = new SSO_Client();

	$sso->is_loggedin();

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Regras | PLR<?php echo date('Y'); ?></title>
	<link href="./favicon.png" rel="shortcut icon" />
	<link href="./assets/css/plr2014.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./assets/js/plr2014.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.Modal.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout.mis" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php $main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content">
			<div class="inner">

			<div class="clear"></div>
  				<h1>Regras</h1>

				<h2 style="font-weight:normal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna sem, adipiscing at nibh a, pellentesque tincidunt tortor. In molestie tristique ante, ac scelerisque diam fringilla nec. Nunc dignissim, arcu et luctus varius, nibh dui laoreet dui, id varius nisi lorem at nunc. Sed ultrices fermentum ligula ac mattis. Ut justo libero, adipiscing eu ante quis, posuere aliquet ipsum. Nullam sollicitudin diam eget enim blandit dictum. Nullam a sapien vel magna mollis hendrerit non eu urna. Pellentesque tincidunt ligula tortor, ut convallis elit pharetra in. In erat nibh, dignissim a mi sit amet, faucibus sollicitudin sapien. In rhoncus velit id eros sollicitudin, eu varius lacus tincidunt. Ut mattis, elit condimentum rutrum dapibus, arcu ipsum lacinia mauris, eu fringilla lorem ligula sed magna. Vestibulum ultricies feugiat molestie. Sed at facilisis magna. Donec varius dolor et lorem ultricies mollis. Donec vel leo turpis. Integer commodo dapibus elit sodales pulvinar.</h2>
			    
				<h1>Metas</h1>
			    
				<h2 style="font-weight:normal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna sem, adipiscing at nibh a, pellentesque tincidunt tortor. In molestie tristique ante, ac scelerisque diam fringilla nec. Nunc dignissim, arcu et luctus varius, nibh dui laoreet dui, id varius nisi lorem at nunc. Sed ultrices fermentum ligula ac mattis. Ut justo libero, adipiscing eu ante quis, posuere aliquet ipsum. Nullam sollicitudin diam eget enim blandit dictum. Nullam a sapien vel magna mollis hendrerit non eu urna. Pellentesque tincidunt ligula tortor, ut convallis elit pharetra in. In erat nibh, dignissim a mi sit amet, faucibus sollicitudin sapien. In rhoncus velit id eros sollicitudin, eu varius lacus tincidunt. Ut mattis, elit condimentum rutrum dapibus, arcu ipsum lacinia mauris, eu fringilla lorem ligula sed magna. Vestibulum ultricies feugiat molestie. Sed at facilisis magna. Donec varius dolor et lorem ultricies mollis. Donec vel leo turpis. Integer commodo dapibus elit sodales pulvinar.</h2>
			    
			    <h1>Faixas de ganho</h1>

			    <h2 style="font-weight:normal">
			    	<img style="width:1em;margin:1em" src="assets/img/3.svg"> 120%  
			    	<img style="width:1em;margin:1em" src="assets/img/2.svg"> 100% 
					<img style="width:1em;margin:1em" src="assets/img/1.svg"> 80%  
			    	<img style="width:1em;margin:1em" src="assets/img/0.svg"> 0%   
			    </h2>

			    <h1>Quem é elegível?</h1>

			    <h2 style="font-weight:normal"><p>Para informações adicionais as regras estão disponíves para <a href="arquivos/regras.pdf">download</a></p></h2>
     		 <div style="height:2em"></div>
     		 <div class="clear"></div>

			</div>
		</div>

		</div>

		<div class="footer">
			<div class="inner">© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
	</div>
</body>

</html>