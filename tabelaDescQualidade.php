<?php

	require 'src/ResultadosUsuario.php';

	if(!isset($_GET['matricula']) or $_GET['matricula'] == ''){
		echo "Definir matricula";
	}
	else{

		$resultados = new ResultadosUsuario($_GET['matricula']);
		echo "<a id='matricula' style='display:none;'>".$_GET['matricula']."</a>";
		echo $resultados->retornaDescQualidade($_GET['id']); 
	}