<?php

	require 'src/ResultadosUsuario.php';

	if(empty($_GET['matricula'])){
		echo "Definir matricula";
	}
	else if(empty($_GET['id'])){
		echo "Definir id do resultado";
	}
	else{
		$resultados = new ResultadosUsuario($_GET['matricula']);
		echo "<a id='matricula' style='display:none;'>".$_GET['matricula']."</a>";
		echo $resultados->retornaDescFaltas($_GET['id']);
	}