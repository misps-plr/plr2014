<?php

	require 'sso/functions.php';
	require 'src/ResultadosReport.php';
	
	if(empty($_GET['report'])){
		echo "Definir matricula";
		exit();
	}
	else{
		$sso = new SSO_Client();
		$resultados = new ResultadosReport($_GET['report']);

		$sso->is_loggedin();
		
		echo "<span id='report_id' style='display:none;'>{$_GET['report']}</span>";
		echo $resultados->retornaResultado();
		#echo $resultados->alomaua();
	}


	