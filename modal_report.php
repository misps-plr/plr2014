<?php

	#
	require './sso/functions.php';
	require './src/Plr.php';

	# Instancia as classes
	$plr = new Plr();
	$sso = new SSO_Client();

	# Checa a existencia de parametros necessarios, vindo via url
	$plr->tem_parametro($_GET['vis']);
	$plr->tem_parametro($_GET['rid']);

	# Camada de Dados
	$plr->set('report_id', $_GET['rid']); # Seta parametros para serem usados pela classe
	$report = $plr->dados_report($plr->report_id); # Variável contendo o vetor com os dados do report
	$crazyTable = $plr->insane($plr->report_id); # 

	# Distinção de visualização
	switch($_GET['vis']){

		# Modal 1 (Analista do MIS inicia o tratamento do report)
		case 'rpt1':
			# Camada de Apresentação
			require './views/modal/report_1.php';
		break;

		# Modal 2 (Analista do MIS responde o report dando uma solução)
		case 'rpt2':
			# Camada de Apresentação
			require './views/modal/report_2.php';
		break;

		# Modal 3 (Analista do MIS consulta report solucionado)
		case 'rpt3':
			# Camada de Apresentação
			require './views/modal/report_3.php';
		break;

	}

	

	

?>