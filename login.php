<?php 

require './sso/functions.php';

	$sso = new SSO_Client();

	
	$badge = (!empty($_COOKIE['lf'])) ? './assets/img/dados_incorretos.png' : './assets/img/badge.png' ;
	$input = (!empty($_COOKIE['lf'])) ? 'error' : 'text' ; 


?>

<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login | PLR 2014</title>
	<link href="./favicon.png" rel="shortcut icon" />
	<link href="./assets/css/login.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./assets/js/plr2014.js"></script>
	<script type="text/javascript" src="./assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.Modal.min.js"></script>
</head>
<body>
	<!-- LOGIN CONTAINER -->
	<div class="container-login">
		<div class="barra-usuario"></div>
		<div class="conteudo">
			<div class="caixa-login">
				<div class="cabecalho-caixa">
					<a href="./login.php"><img src="./assets/img/logo_full.png" /></a>
				</div>
				<div class="conteudo-caixa" >
					<div class="flutua-dir cc-direita">
						<form action="<?php echo $sso->get_config('server_url','login'); ?>" method="POST" >
							<input type="text" name="user" placeholder="Matrícula" class="<?php echo $input; ?>" required autofocus /><br/>
							<input type="password" name="pass" placeholder="Senha" class="<?php echo $input; ?>" required /><br/>
							<input type="submit" value="ENTRAR" class="submit" />
							<input type="hidden" name="service" value="<?php echo $sso->get_config('security','service_id'); ?>" />
						</form>
					</div>
					<div class="flutua-dir cc-esquerda">
						<img alt="badge" id="cracha" src="<?php echo $badge; ?>" />
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div><!-- /fim conteudo -->

		<div class="rodape">
		</div><!-- /fim footer -->
	</div>
	<!-- LOGIN CONTAINER -->
	
	<?php if(!empty($_COOKIE['lf'])){ ?>
	<script type="text/javascript">
		$(function(){
			$.Notify({
				content: "<?php echo $sso->read_cookie('lf');?>",
				shadow: true,
				style: {background: 'red', color: 'white'},
				timeout: 5000
			});

			//$('#cracha').attr('src', './assets/img/badge.png')
		});
	</script>
	<?php } ?>


</body>
</html>