
// Roda o tempo todo
function everyTime(){
	setInterval(function(){
		endLifetimeSessionNotify()
	},1000);
}

// grava cookie
function setCookie(name, value){
	var expires;
	var date; 
	var value;

	date = new Date(); //  criando o COOKIE com a data atual
	date.setTime(date.getTime()+(1*24*60*60*1000));
	expires = date.toUTCString();
	document.cookie = name+"="+value+"; expires="+expires+"; path=/";
}

// recupera cookie
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }

    return null;
}

// limpa o elemento hmtl
function clear_element(ident){
	$(ident).empty();
}

// array with options to hide
function toHide(elements){
	for(var i = 0; i <= elements.length-1; i++){
		document.getElementById(elements[i]).style.display='none';
	}
}


// array with options to show
function toShow(elements){
	for(var i = 0; i <= elements.length-1; i++){
		document.getElementById(elements[i]).style.display='block';
	}
}


// refactor
function sessionLifetime(){
	time 		= new Date();
	current 	= time.getTime();
	lifetime 	= window.atob(getCookie('tm')) * 1000; //miliseconds ;)
	difference 	= lifetime - current;
	
	return Math.floor(difference / 1000 / 60);
}


// refactor
function endLifetimeSessionNotify(){
	time 	= new Date();
	sec 	= time.getSeconds();

	if(sec == 1){
		if(sessionLifetime() === 10){
			notification('error', getCookie('jsnm') + ', sua sessão irá expirar em ' + timeToString(sessionLifetime()) + ' minutos!', 10000);
		}

		if(sessionLifetime() === 5){
			notification('error', getCookie('jsnm') + ', sua sessão irá expirar em ' + timeToString(sessionLifetime()) + ' minutos!', 10000);
		}

		if(sessionLifetime() === 3){
			alert(getCookie('jsnm') + ', sua sessão irá expirar em ' + timeToString(sessionLifetime()) + ' minutos!');
		}

		if(sessionLifetime() === 0){
			window.location.reload();
		}
	}
}

// 
function timeToString(time){
	var minutes = time % 60;
	var hours = Math.floor(time  / 60);

	return (hours === 0) ? minutes : hours + ':' + (minutes < 10 ? '0' : '') + minutes ;
}


// Ajax #refactor
function objxml(){
	var objxml;
	if (window.XMLHttpRequest) {
		var objxml = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
	}
	else{
		var objxml = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
	}

	return objxml;
}

// build
function saveTab(tab){
	setCookie('tab', tab);
	//console.log(tab);
}

function showTab(){
	(getCookie('tab') === null)  ? $('.rp-new').attr('class', 'active') : $('.rp-' + getCookie('tab')).attr('class', 'active');
}


// Data Tables
function init_dataTable(element, orderby){
	$(element).dataTable({
		'sPaginationType': 'simple',
		'lengthMenu': [15, 30, 60],
		'language': {
		    'info': 'Página _PAGE_ de _PAGES_ (_TOTAL_ Resultados)',
		   	'infoEmpty': '',
		   	'infoFiltered': "",
		   	'lengthMenu': 'Exibir _MENU_ colaboradores',
		    'paginate': {
		    	'next': 'Próxima',
		    	'previous': 'Anterior'
		   	},
		   	'search': '',
		   	'zeroRecords': 'Não foram encontrados colaboradores com este critério de pesquisa.'
		},
		'order': [[ orderby, 'desc' ]]
	});
}

// Reports
//
function popula_modal_report(report_id){
	is_loggedin();
}

//
function repopula_modal_report(report_id){
	$.ajax({
		crossDomain: 'true',
		type: 'GET',
		url: window.location.origin+'/plr2014/modal_report?vis=rpt2&rid='+report_id,
		beforeSend: function(){
			$('.modal-content').html('Carregando...')
		},
		success: function(result){
 			$('.modal-content').html(result); // escreve conteudo vindo via ajax
		}
	});
}

function tratar_report(){
	is_loggedin();

	var xmlhttp = objxml();
	var campos  = new FormData();

	var report_id = document.getElementById('report_id').value;
	var analyst_id = document.getElementById('analyst_id').value;

	campos.append('report_id', report_id);
	campos.append('analyst_id', analyst_id);

	xmlhttp.open("POST", window.location.origin + '/plr2014/pringles?vis=rp_atend');
	xmlhttp.send(campos);

	clear_element('.reports');
	repopula_modal_report(report_id);
}

function responder_report(){
	is_loggedin();

	var retorno = null;
	var xmlhttp = objxml();
	var campos  = new FormData();

	//
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			retorno  = xmlhttp.responseText;
	  		mensagem = retorno.split(';')[1];
	  		tipo     = retorno.split(';')[0];

			notification(tipo, mensagem);
			setInterval(function(){ window.location.reload(); }, 1000);
		}
	}

	//
	var comment = document.getElementById('comment').value;
	var report_id = document.getElementById('report_id').value;
	var analyst_id = document.getElementById('analyst_id').value;


	campos.append('comment', comment);
	campos.append('report_id', report_id);
	campos.append('analyst_id', analyst_id);

	xmlhttp.open("POST", window.location.origin + '/plr2014/pringles?vis=rp_resp');
	xmlhttp.send(campos);
}

function apagar_report(report_id){

}




//

function breadcrumb(dsc){
	if(dsc==''){
		document.getElementById('breadcrumb').innerHTML='';
	}
	
	if(dsc!=''){
		document.getElementById('breadcrumb').innerHTML=dsc;
	}
}

function notification(type, message, timeout) {
	var colors	 = {error:'#ff1a1a', success:'#64c800', info:'#d9edf7', warning:'#fcf8e3'};
	var timeout  = (timeout === undefined || timeout === null) ? 5000 : timeout ;
	var bg_color = colors[type];


   	$.Notify({
        content: message,
        timeout: timeout
    });

    $('.notify').css({
    	'background-color': bg_color,
    	'color': '#fff',
    	'display': 'block',
    	'font-family': '\'Segoe UI_\', \'Open Sans\', Verdana, Arial, Helvetica, sans-serif',
    	'margin': '2px 2px 2px 0',
    	'padding': '10px',
    	'text-transform': 'uppercase'
    });

    $.modal.close();
}



function submitForm(action){
	document.getElementById('resultadosForm').action = action;
	document.getElementById('resultadosForm').submit();
}

function acaoValidados(action){
	// verifica se o candango está logado antes de agir
	is_loggedin();

	var xmlhttp = objxml()
	var campos = new FormData();
	var uid = document.getElementById('matricula').innerHTML;
	var retorno;

	xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	  		retorno  = xmlhttp.responseText
	  		mensagem = retorno.split(';')[1];
	  		tipo     = retorno.split(';')[0];

	  		notification(tipo, mensagem);
	  		tabelaResultados(uid);
		}
	}

	checkbox = document.getElementsByTagName('input');

	for(i = 0; i < checkbox.length; i++) {
		inputTemp = checkbox[i];

		campoTemp = inputTemp.name.split("-");
		
		if(campoTemp[1] == action && inputTemp.disabled == false && inputTemp.checked == true){
        	campos.append(campoTemp[0],'TRUE');

        	console.log(campos);
    	}
    }
   
	xmlhttp.open("POST", window.location.origin+"/plr2014/valida.php?matricula=" + uid + "");
	xmlhttp.send(campos);
}



reportados = 0;
function acaoReportados(action){
	reportados++;

	is_loggedin(); 
	
	var retorno = null;
	var xmlhttp = objxml()
	var campos = new FormData();
	var matricula = document.getElementById('matricula').innerHTML;

	xmlhttp.onreadystatechange = function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	  		retorno  = xmlhttp.responseText
	  		mensagem = retorno.split(';')[1];
	  		tipo     = retorno.split(';')[0];

	  		notification(tipo, mensagem);
	  		tabelaResultados(matricula);
		}
	}

	checkbox = document.getElementsByTagName('input');

	for(i = 0; i < checkbox.length; i++) {
		inputTemp = checkbox[i];

		campoTemp = inputTemp.name.split("-");
		
		if(campoTemp[1] == action && inputTemp.disabled == false && inputTemp.checked == true){
        	campos.append(campoTemp[0],'resultado');
    	}
    }

    campos.append('comment', document.getElementById('comment').value);
    campos.append('uid', document.getElementById('uid').value);


    if(reportados == 1){
   		xmlhttp.open("POST", window.location.origin+"/plr2014/reporta.php?matricula=" + matricula + "");	
		xmlhttp.send(campos);
	}

	if(reportados > 2){
		alert(getCookie('jsnm') + ', o resultado já foi reportado!');
		return false;
	}

}

// botao responder report
function acaoRespostaReport(){
	// verifica se o candango está logado antes de agir
	is_loggedin(); 
	
	//
	var xmlhttp = objxml()
	var campos = new FormData();
	var report_id = document.getElementById('report_id').innerHTML;
	var retorno;

	//
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			retorno  = xmlhttp.responseText;
	  		mensagem = retorno.split(';')[1];
	  		tipo     = retorno.split(';')[0];

			notification(tipo, mensagem);

			setInterval(function(){ window.location.reload(); }, 1000);
		}
	}

	//
	campos.append('rid', report_id);
	campos.append('mis', document.getElementById('analista_mis').value);
	campos.append('comment', document.getElementById('comment').value);

	//    
  	xmlhttp.open("POST", window.location.origin+"/plr2014/resposta_report.php");	
	xmlhttp.send(campos);


}

function consultaQualidade(matr,idResultado){
	var xmlhttp = objxml();

	xmlhttp.onreadystatechange = function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	  		document.getElementById('kpi-result-description').innerHTML = xmlhttp.responseText;

			hide = ['results-overview','aviso-overview','default-actions'];
			show = ['kpi-result-description','kpi-description-actions'];

			toHide(hide);
			toShow(show);
		}
	}

	xmlhttp.open("GET", window.location.origin+"/plr2014/tabelaDescQualidade.php?matricula="+matr+"&id="+idResultado);
	xmlhttp.send();

}

function consultaFalta(matr,idResultado){
	var xmlhttp = objxml();

	xmlhttp.onreadystatechange = function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	  		document.getElementById('kpi-result-description').innerHTML = xmlhttp.responseText;

			hide = ['results-overview','aviso-overview','default-actions'];
			show = ['kpi-result-description','kpi-description-actions'];

			toHide(hide);
			toShow(show);
		}
	}

	xmlhttp.open("GET", window.location.origin+"/plr2014/tabelaDescFaltas.php?matricula="+matr+"&id="+idResultado);
	xmlhttp.send();

}

function botaoVoltar(){
	hide = new Array('kpi-result-description','kpi-description-actions');
	show = new Array('results-overview','default-actions');

	toHide(hide);
	toShow(show);
}


// tem resultado para reportar ou validar?
function temResultados(acao){
	nomeInput = (acao == 'REPORTAR') ? 'REPORTADO' : 'VALIDADO' ; //XPTO
	input = document.getElementsByTagName('input');

	// Percorre todos os inputs da pagina
	for(i=0; i<input.length; i++){
		checkbox = input[i];

		// Apenas 'checkbox' com data-action="XPTO"
		if(checkbox.type.toLowerCase() == 'checkbox' && checkbox.dataset.action == nomeInput){
			if(!checkbox.disabled){ // tem checkbox disponiveis para serem marcados?
				return true;
			}
		}
    }
}

// o resultado esta checkado?
function resultadoCheckado(acao){
	nomeInput = (acao == 'REPORTAR') ? 'REPORTADO' : 'VALIDADO' ; //XPTO
	input = document.getElementsByTagName('input');

	// Percorre todos os inputs da pagina
	for(i=0; i<input.length; i++){
		checkbox = input[i];

		// Apenas 'checkbox' com data-action="XPTO"
		if(checkbox.type.toLowerCase() == 'checkbox' && checkbox.dataset.action == nomeInput){
			if(!checkbox.disabled){ // tem checkbox disponiveis para serem marcados?
				//
				if(checkbox.checked){
					return true;
				}
			}
		}
    }
}


function botaoReportar(){
	alert(getCookie('jsnm') + ', o horário para lançar os reports finalizou em 27/01/2015 às 17:30!');

	/*
	// verifica se o candango está logado antes de agir
	is_loggedin();

	// verifica se existe resultados para serem reportados
	if(temResultados('REPORTAR')){
		// verifica se o resultado foi checkado antes de ser reportado
		if(resultadoCheckado('REPORTAR')){ // exibe a div
			show = ['report-comments','reported-actions'];
			hide = ['results-overview','aviso-overview','default-actions'];
			
			toHide(hide);
			toShow(show);
		}
		else{ // não foi selecionado nenhum resultado pra reportar
			notification('error', getCookie('jsnm')+', você precisa escolher o(s) resultado(s) a ser(em) reportado(s)!'); // mensagem pro cabaço!	
		}
	}
	else{ // diz que nao tem resultado para reportar
		notification('error', 'Não Existem Resultados Passíveis de Serem Reportados!'); // mensagem pro cabaço!
	}*/
}

function botaoCancelar(){
	//
	hide = new Array('report-comments','reported-actions');
	show = new Array('results-overview','aviso-overview','default-actions');

	//
	toHide(hide);
	toShow(show);
}


function tabelaResultados(matricula){
	var xmlhttp = objxml()
	var uid = document.getElementById('matricula').innerHTML;

	xmlhttp.onreadystatechange=function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("callback").innerHTML = xmlhttp.responseText;
		}
	}

   	xmlhttp.open("GET", window.location.origin+"/plr2014/tabela.php?matricula=" + matricula + "");
	xmlhttp.send();
}

function agentesPorFaixa(gerencia){
	
	var xmlhttp = objxml()

	xmlhttp.onreadystatechange = function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	  		jsonTemp = JSON.parse(xmlhttp.responseText)
	  		localStorage.setItem("ind1",'[' + jsonTemp.ind1 + ']'); 
	  		localStorage.setItem("ind2",'[' + jsonTemp.ind2 + ']');
	  		localStorage.setItem("ind3",'[' + jsonTemp.ind3 + ']');
	  		localStorage.setItem("ind4",'[' + jsonTemp.ind4 + ']');
			consultaComparativo(1,gerencia);
		}
	}

	xmlhttp.open("GET", window.location.origin+"/plr2014/agentesPorFaixas.mis?gerencia=" + gerencia);
	xmlhttp.send();


}

function consultaComparativo(indicador){

	var nodes = document.getElementById('indicadores-seletor').childNodes;
	for(var i=0; i<nodes.length; i++) {
	    nodes[i].className = '';
	}

	document.getElementById("ind"+indicador).className = "active";

	var xmlhttp = objxml();

	xmlhttp.onreadystatechange = function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	  		document.getElementById('comparativo').innerHTML  = xmlhttp.responseText;
		}
	}

	resultados = JSON.parse(localStorage['ind' + indicador]);

	xmlhttp.open("GET", window.location.origin+"/plr2014/comparacao.php?0= " + resultados[0] + "&&80= " + resultados[1] + "&&100=" + resultados[2] + "&120="+ resultados[3] +"");
	xmlhttp.send();

}

function is_loggedin(){
	if(getCookie('plr2014') == null){
		document.location.reload();
	}
}



