<?php

	#
	require './sso/functions.php';
	require './src/Plr.php';

	# Instancia as classes
	$plr = new Plr();
	$sso = new SSO_Client();

	# Checa a existencia de parametros necessarios, vindo via url
	$plr->tem_parametro($_GET['vis']);

	# Distinção de visualização
	switch($_GET['vis']){
		case 'rp_atend':
			# Camada de Dados
			$report_id = $_POST['report_id'];
			$analyst_id = $_POST['analyst_id'];

			# Muda o status do report
			$plr->muda_status_report($report_id, $analyst_id, 2);
		break;

		case 'rp_resp':
			$comment = $_POST['comment'];
			$report_id = $_POST['report_id'];
			$analyst_id = $_POST['analyst_id'];
			
			$plr->responder($report_id, $comment, $analyst_id);
		break;
	}

	

?>