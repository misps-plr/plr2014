<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Navegador Incompatível</title>
		<link href="./favicon.png" rel="shortcut icon" />
		<link href="http://172.23.14.155/plr2014/assets/css/login.style" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<!-- LOGIN CONTAINER -->
		<div class="container-login">
			<div class="barra-usuario"></div>
			<div class="conteudo">
				<div class="caixa-login">
					<div class="cabecalho-caixa">
						<a href="./"><img src="./assets/img/logo_full.png" /></a>
					</div>
					<div class="conteudo-caixa">
						<div class="flutua-dir cc-direita">
							<p style="color:white; margin-bottom: 20px">Parece que você esta utilizando um navegador que não suporta nossos sistemas.</p>
							<p style="color:white;">Para acessar o sistema, e utilizar todas as funcionalidades utilize o <a style="color:white;text-decoration: underline;" href="http://www.google.com/intl/pt-BR/chrome/browser/" target="_blank">Google Chrome</a></p>
						</div>
						<div class="flutua-dir cc-esquerda">
							<img alt="badge" id="cracha" src="./assets/img/navegador.png" />
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div><!-- /fim conteudo -->

			<div class="rodape">
			</div><!-- /fim footer -->
		</div>

	</body>
</html>