<?php

	require 'sso/functions.php';
	require 'src/ResultadosUsuario.php';
	
	if(empty($_GET['matricula'])){
		echo "Definir matricula";
	}
	else{
		$sso = new SSO_Client();
		$resultados = new ResultadosUsuario($_GET['matricula']);

		$sso->is_loggedin();

		echo "<a id='matricula' style='display:none;'>".$_GET['matricula']."</a>";
		echo $resultados->retornaResultado();
	}