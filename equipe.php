<?php

	require './src/functions.php';
	require './sso/functions.php';

	$main = new Main();
	$sso = new SSO_Client();

	$sso->is_loggedin();

	$breadcrumb = 'Resultados da Equipe';
	$manager = ($sso->get_user_data('manager') == '0') ? 0 :$sso->get_user_data('manager');

	$json = json_decode(file_get_contents('./data/gerencia_'.$manager.'.json'), true);
	
?>

<!doctype html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Resultados da Equipe | PLR 2014</title>
	<link href="./favicon.png" rel="shortcut icon" />
	<link href="./assets/css/plr2014.style" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.Modal.min.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.min.js"></script>
	<script type="text/javascript" src="./assets/js/metro.Notify.min.js"></script>
	<script type="text/javascript" src="./assets/js/plr2014.js"></script>
</head>
<body>
	<div class="container">
		<div class="notify-container">
			<div class="notify shadow" style="background: transparent !important;" id="notifyid"></div>
		</div>
		<!-- /end notify -->

		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout.mis?continue=<?php echo $sso->get_current_url();?>" class="btn-logout" title=""><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							<li class="fr">
								<a href="./reportados" class="btn-nav-sup" title="Resultados Reportados" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-wrench"></i>
								</a>
							</li>
							<li class="fr">
								<a href="./equipe" class="btn-nav-sup" title="Resultados da Equipe" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');" >
									<i class="icon-stats-up"></i>
								</a>
							</li>
							<?php if($sso->get_user_data('type') == 2){ ?>
							<li class="fr">
								<a href="./admin/index" class="btn-nav-sup" title="Administrar PLR" onmouseover="breadcrumb(this.title);" onmouseout="breadcrumb('');">
									<i class="icon-dashboard"></i>
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php echo $breadcrumb; ?></p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">
				
				<div class="metro-datatable">
					<table class="table striped bordered hovered dataTable" id="employees">
						<thead>
							<tr>
								<th class="uppercase">nome</th>
								<th class="uppercase span2">matrícula</th>
								<th class="uppercase span2">cargo</th>
								<th class="uppercase span2">célula</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($json as $row){ ?>
							<tr>
								<td class="capitalized "><a data-registration="<?php echo $row['uid'];?>" href="#show"><?php echo $row['full_name'];?></a></td>
								<td class="capitalized text-center"><?php echo $row['uid'];?></td>
								<td class="capitalized text-center"><?php echo $row['job_position'];?></td>
								<td class="capitalized text-center"><?php echo $row['work_cell'];?></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
				<!-- /userlist -->
				<div class="clear"></div>
			</div>
			<!-- /end inner -->
		</div>
		<!-- /end content -->

		<div class="footer">
			<div class="inner">© <?php echo (date('Y') == 2014) ? date('Y') : '2014 - '.date('Y') ;?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
		<!-- /end footer -->


		<div class="modal-container employee-data">
			<div class="modal-content"></div>
		</div>
		<!-- /end modal -->

		<div class="modal-container loading-spinner">
			<div id="floatingBarsG">
				<div class="blockG" id="rotateG_01"></div>
				<div class="blockG" id="rotateG_02"></div>
				<div class="blockG" id="rotateG_03"></div>
				<div class="blockG" id="rotateG_04"></div>
				<div class="blockG" id="rotateG_05"></div>
				<div class="blockG" id="rotateG_06"></div>
				<div class="blockG" id="rotateG_07"></div>
				<div class="blockG" id="rotateG_08"></div>
			</div>
		</div>
		<!-- /end modalspinner -->

		<div class="notify-container">
			<div class="notify shadow" style="background: transparent !important;" id="notifyid"></div>
		</div>
		<!-- /end notify container -->

	</div>

	<script type="text/javascript">
		//
		window.onload = function(){
			everyTime();
		};

		// Jquery Feelings
		$(document).ready(function(){

			// DataTable
			$('#employees').dataTable({
				'sPaginationType': 'simple',
				'lengthMenu': [15, 30, 60, 120, 250, 500],
				'language': {
				    'info': 'Página _PAGE_ de _PAGES_ (_TOTAL_ Resultados)',
				   	'infoEmpty': '',
				   	'infoFiltered': "",
				   	'lengthMenu': 'Exibir _MENU_ colaboradores',
				    'paginate': {
				    	'next': 'Próxima',
				    	'previous': 'Anterior'
				   	},
				   	'search': '',
				   	'zeroRecords': 'Não foram encontrados colaboradores com este critério de pesquisa.'
				},
				//'order': [[ 2, 'desc' ]]
			});


			// Modal
			$('#employees').on('click', 'a[href="#show"]', function(event){
				// obtem a matricula do candango
				var registration = $(this).data('registration');

				// requisição ajax
				$.ajax({
					crossDomain: 'true',
					type: 'GET',
					url: window.location.origin+'/plr2014/tabela.php?matricula='+registration, 
					beforeSend: function(){
						$('.loading-spinner').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							showClose: false
						});
					},
					success: function(result){
						$('.loading-spinner').hide();
		     			$('.modal-content').html(result); // escreve conteudo vindo via ajax

		     			// seta a altura do modal
		     			var top = ($(window).height()/2) - ($('.modal').height()/2);

		     			// reescreve o css com a nova altura
						$('.modal').css({
							'margin-top': 0, /* hackzinho maroto _|,,/ */
							'top': top
						});

						// cria o modal
						$('.employee-data').modal({
							fadeDuration: 250,
							fadeDelay: 1.5,
							keyboard: true,
							showClose: true
						});
		   			}
		   		}); // fim ajax-jquery

				is_loggedin();
			});

		}); // Jquery Ready function
	</script>

	
	<div id="shazam"></div>
</body>
</html>