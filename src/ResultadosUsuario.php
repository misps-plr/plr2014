<?php
	
	#require 'sso/functions.php';
	require 'Plr.php';

	/**
	* Classe que busca os resultados do agente, recebe como argumento a matricula
	*/
	class ResultadosUsuario extends Plr{
		
		private $elegivel;
		private $resultados;

		function __construct($matricula){
			$this->usuario = new Usuario($matricula);
			$this->conn = $this->usuario->conn;
			$this->matricula = $this->usuario->getMatricula();
			$this->elegivel = $this->usuarioElegivel();
			$this->resultados = $this->usuarioElegivel() ?  $this->resultados() : NULL;
		}

		function usuarioElegivel(){
			return odbc_fetch_array($this->conn->executaRetorno("SELECT * FROM plr_2014.proc_elegivel('".$this->matricula."')"));
		}

		function resultados(){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_consulta('$this->matricula')");
		}

		function resultadosDescQualidade($id){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_desc_qualidade(".$id.",'".$this->matricula."')");
		}

		function resultadosDescFalta($id){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_desc_faltas(".$id.",'".$this->matricula."')");
		}

		function trataValores($results,$key,$id,$results_ant){

			$simbol = '';

			if ($results == '' or $results == '-1' or $results == '-1%-1' or $results == '-1-1') {
				$results = '-';
			}

			if($key == 'mes' and $results == '0_0'){
				$results = 'Total';
			}

			if((($key == 'validado' and $results <> 'Validacao') or ($key == 'reportado' and $results <> 'Reportado')) and $results <> '-'){
				$checked = '';

				if($results == 'true')
					$checked = 'checked disabled';

				if($results_ant == 'true')
					$checked = 'disabled';

				$results = "<input type='checkbox' data-action='".$key."' name='".$id."-".$key."' value='true'  $checked >";
			}

			if(sizeof(split("_",$results)) == 2 and split("_",$results)[1] <> '-1' and $key <> 'mes'){
				$simbol = "<img class='' height='15px' src='assets/img/".split("_",$results)[1].".svg'/>";
				$results = str_replace('_', '', split("_",$results)[0]);
			}elseif($key == 'mes'){
				if(str_replace('_', '', split("_",$results)[1]) <> '')
					$results = str_replace('_', '', split("_",$results)[1]);
			}elseif (sizeof(split("_",$results)) == 2 and split("_",$results)[1] == '-1') {
				$results = str_replace('_', '', split("_",$results)[0]);
			}

			return array($results,$simbol);
		
		}

		function retornaResultado(){
			global $sso;
			$sso->is_loggedin(); # resolve bug que permite acessar a tela de resultados sem estar logado

			$uid = $sso->get_user_data('uid');

			if($this->elegivel){

				$rs = $this->resultados;

				$html  = "<form id='resultadosForm' method='post'>";
				
				$html .= "<div class=\"employee-name\">";
				$html .= "	<h1 class=\"uppercase\">{$this->usuario->getNome()}</h1>";
				$html .= "	<div class=\"clear\"></div>";
				$html .= "</div>";
				$html .= '<div class="results-table scroll" id="results-overview">';
				$html .= '<table class="table" >';

				while($array = odbc_fetch_array($rs)){

					$id = current($array);
					$nivel = next($array);
					
					$html .= "<tr id='".$id."' class = 'row results-".$nivel."'>";
						
					next($array);

					$results_ant = '';

					$w = 0;
					$j = 0;

					while (key($array)) {

						if($nivel == '0' and substr(key($array),0,3) == 'ind'){
							$indicadores[$j] = current($array);
						}

						if (current($array) <> '-') {
						
							$results = $this->trataValores(current($array),key($array),$id,$results_ant);
						
							$action = 'class="fl"';

							if($indicadores[$j] == 'qualidade' and $nivel <> '0' and $nivel <> '1'){
								$action = "class='fl kpi-description' onclick='consultaQualidade(".$this->matricula.",".$id.")'";
							}
							else if($indicadores[$j] == 'faltas' and $nivel <> '0' and $nivel <> '1'){
								$action = "class='fl kpi-description' onclick='consultaFalta(".$this->matricula.",".$id.")'";
							}

							$html .= '<td class="row-content">';
							$html .= '<span '.$action.'>'.strtoupper($results[0]).'</span>';
							$html .= '<span class="fl rank-position">'.$results[1].'</span>';
							$html .= '</td>';

							$results_ant = current($array);
						
						}
						
						next($array);
						$j++;
					}

					$html .= "</tr>";

				}
				
				$html .= '</table>';
			    $html .= '</div>';

			    # Quadro de Avisos 
			    $html .= '<div class="avisos" id="aviso-overview">';
			    $html .= '<h3 style="font-weight:normal"> 
			    			Faixas de ganho:
					    	<img style="width:1em;margin:.5em" src="assets/img/3.svg"> 120%  
					    	<img style="width:1em;margin:.5em" src="assets/img/2.svg"> 100% 
							<img style="width:1em;margin:.5em" src="assets/img/1.svg"> 80%  
					    	<img style="width:1em;margin:.5em" src="assets/img/0.svg"> 0%   
					    </h3>';
				$html .= '<h3 style="font-weight:normal"> * O indicador de qualidade é calculado utilizando a média ponderada, assim consideramos também o número de monitorias em cada um dos meses no cálculo.</h3>';
			    $html .= '</div>';

			    # Botoes
				$html .= '<div class="actions fr" id="default-actions">';
				$html .= '	<input type="button" onclick="acaoValidados(this.id)" id="VALIDADO" value="Validar" class="button fr success">';
				$html .= '	<input type="button" onclick="botaoReportar()" id="REPORTADO" value="Reportar" class="button fr" style="margin-right:5px;">';
				$html .= '</div>';

				# Reports
				$html .= '<div class="report-comments" id="report-comments" style="display:none">';
				$html .= '<textarea class="comment-area" id="comment" name="comment" placeholder="DIGITE O PROBLEMA APRESENTADO NOS RESULTADOS!" autofocus required></textarea>';
				$html .= '</div>';

				$html .= '<div class="actions fr" id="reported-actions" style="display:none">';
				$html .= '	<input type="button" onclick="acaoReportados(this.id)" id="REPORTADO" value="Reportar" class="button fr success">';
				$html .= '	<input type="button" onclick="botaoCancelar()" value="Cancelar" class="button fr" style="margin-right:5px;">';
				$html .= '</div>';

				# KPI Descriptions
				$html .= '<div class="" id="kpi-result-description" style="display:none;">Alomaua</div>';
				$html .= '<div class="actions fr" id="kpi-description-actions" style="display:none;">';
				$html .= '	<input type="button" onclick="botaoVoltar()" id="VOLTAR" value="Voltar" class="button fr success">';
				$html .= '</div>';

				$html .= '<input type="hidden" id="uid" name="uid" value="'.$uid.'">';
				$html .= '</form>';
				$html .= '<div class="clear"></div>';

				return $html;

			}
			else{
				// Cara tu nao é elegivel à plr por esta sem resultado
				return "Sem resultados";

			}
		}


		function retornaDescQualidade($id){
			//$sso = new SSO_Client();
			//$sso->is_loggedin(); # resolve bug que permite acessar a tela de resultados sem estar logado
			//$uid = $sso->get_user_data('uid');

			if($this->elegivel){

				$rs = $this->resultadosDescQualidade($id);
				$html .= '<div class="results-table scroll" id="kpi-results-table">';
				$html .= "<table class='table' >";

				$array = odbc_fetch_array($rs);

				$html .= "<tr class='row results-0'>";
				while (key($array)) {
					
						$html .= '<td class="row-content">';
						$html .= current($array);
						$html .= '</td>';

						$results_ant = current($array);

					next($array);
				}

				$html .= "</tr>";

				$array = odbc_fetch_array($rs);

				$html .= "<tr class='row results-1'>";
				while (key($array)) {
					
						$html .= '<td class="row-content">';
						$html .= current($array);
						$html .= '</td>';

						$results_ant = current($array);

					next($array);
				}

				$html .= "</tr>";

				while($array = odbc_fetch_array($rs)){

					$html .= "<tr class='row results-2'>";
					$results_ant = '';

						while (key($array)) {
							
								$html .= '<td class="row-content">';
								$html .= current($array);
								$html .= '</td>';

								$results_ant = current($array);

							next($array);
						}

					$html .= "</tr>";

				}
				
				$html .= '</table>';
			    $html .= '</div>';

			    $html .= '<div class="avisos " id="aviso-qualidade">';
			    $html .= '</div>';

				$html .= '<div class="clear"></div>';
				return $html;

			}
			else{
				return "Sem resultados";
			}

		}

		function retornaDescFaltas($id){
			//global $sso;
			//$sso->is_loggedin(); # resolve bug que permite acessar a tela de resultados sem estar logado

			if($this->elegivel){

				$rs = $this->resultadosDescFalta($id);

				$html .= '<div class="results-table scroll" id="kpi-results-table">';
				$html .= "<table class='table' >";

				$array = odbc_fetch_array($rs);

				$html .= "<tr class='row results-0'>";
				while (key($array)) {
					
						$html .= '<td class="row-content">';
						$html .= current($array);
						$html .= '</td>';

						$results_ant = current($array);

					next($array);
				}

				$html .= "</tr>";

				$array = odbc_fetch_array($rs);

				$html .= "<tr class='row results-1'>";
				while (key($array)) {
					
						$html .= '<td class="row-content">';
						$html .= current($array);
						$html .= '</td>';

						$results_ant = current($array);

					next($array);
				}

				$html .= "</tr>";

				while($array = odbc_fetch_array($rs)){

					$html .= "<tr class='row results-2'>";
					$results_ant = '';

						while (key($array)) {
							
								$html .= '<td class="row-content">';
								$html .= current($array);
								$html .= '</td>';

								$results_ant = current($array);

							next($array);
						}

					$html .= "</tr>";

				}
				
				$html .= '</table>';
			    $html .= '</div>';

			    $html .= '<div class="avisos " id="aviso-faltas">';
			    $html .= '<h3 style="font-weight:normal"> * Será contabilizada apenas uma falta por dia mesmo que sejam exibidos dois ou mais motivos para tal ausência.</h3>';
			    $html .= '</div>';

				$html .= '<div class="clear"></div>';

				return $html;

			}
			else{
				return "Sem resultados";
			} # fim elegivel


		} # fim do metodo

	}
