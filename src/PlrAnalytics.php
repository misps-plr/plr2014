<?php
	require 'ConexaoBancoMisPg.php';
	require 'iPlrAnalytics.php';
	
	/**
	* Classe para analise dos dados de acesso do sistema
	*/
	class PlrAnalytics implements iPlrAnalytics{

		private $totalDeResultados;
		private $totalDeResultadosValidados;
		private $totalDeReports;
		private $totalDeReportsRespondidos;
		private $totalDeAcessosPorGerencia;
		private $totalDeAcessos;

		function __construct($id_report){
			$this->conn = new ConexaoBancoMisPg();

			$strsql = "
				SELECT 
					COUNT(te.id) total_de_resultados,
					(SELECT COUNT(id) FROM plr_2014.tbl_extrato WHERE validado IS TRUE) total_de_validados,
					(SELECT COUNT(id) FROM plr_2014.tbl_reports) as total_de_reports,
					(SELECT COUNT(id) FROM plr_2014.tbl_reports WHERE cfmis IS NOT NULL) as total_de_reports_respondidos
				FROM plr_2014.tbl_extrato te 
				INNER JOIN plr_2014.tbl_cargos_elegiveis tce ON 
					tce.cargo = te.cargo and tce.ccusto = te.ccusto";

			$arrayTemp = $this->conn->retornaArray($strsql)[0];

			$this->totalDeResultados = $arrayTemp[0];

			$this->totalDeResultadosValidados = $arrayTemp[1];

			$this->totalDeReports = $arrayTemp[2];

			$this->totalDeReportsRespondidos = $arrayTemp[3];

			$strsql = "
				SELECT acessos,gerente FROM plr_2014.tbl_acessos ta INNER JOIN plr_2014.tbl_gerente tger ON tger.id = ta.gerencia ORDER BY acessos DESC";

			$this->totalDeAcessosPorGerencia = $this->conn->retornaArray($strsql);

			$strsql = "
				SELECT SUM(acessos) FROM plr_2014.tbl_acessos";

			$this->totalDeAcessos = $this->conn->retornaArray($strsql)[0][0];
		}

		function getTotalDeResultados(){
			return $this->totalDeResultados;
		}

		function getTotalDeAcessos(){
			return $this->totalDeAcessos;
		}

		function getTotalDeResultadosValidados(){
			return $this->totalDeResultadosValidados;
		}

		function getTotalDeReports(){
			return $this->totalDeReports;
		}

		function getTotalDeReportsRespondidos(){
			return $this->totalDeReportsRespondidos;
		}

		function getPercentualDeValidados(){
			return ($this->totalDeResultadosValidados/$this->totalDeResultados)*100;
		}

		function getPercentualDeReportados(){
			return ($this->totalDeReports/$this->totalDeResultados)*100;
		}

		function getPercentualDeReportsRespondidos(){
			return ($this->totalDeReportsRespondidos/$this->totalDeReports)*100;
		}

		function getReportsPorAcesso(){
			return ($this->totalDeReports/$this->totalDeAcessos);
		}

		function getReportsPorHora(){
			$strsql = "
			SELECT total,hora FROM(
				SELECT 
					date_trunc('hour',data_insert)::time hora,count(date_trunc('hour',data_insert)::time) total
				FROM plr_2014.tbl_reports
				GROUP BY date_trunc('hour',data_insert)::time
			) qr1
			ORDER BY total DESC";

			$arrayTemp = $this->conn->retornaArray($strsql);

			return $arrayTemp;
		}

		function getReportsPorMatricula(){
			$strsql = "
				SELECT total,matricula FROM (
					SELECT 
					COUNT(matricula) total,matricula 
					FROM plr_2014.tbl_reports
					GROUP BY matricula
				) qr1
				ORDER BY total DESC
				LIMIT 10";

			$arrayTemp = $this->conn->retornaArray($strsql);

			return $arrayTemp;
		}

		function getReportsPorDiaDaSemana(){
			$strsql = 	"
			SELECT total,semana FROM(
				SELECT 
					extract('DOW' from data_insert) semana,count(extract('DOW' from data_insert)) total
				FROM plr_2014.tbl_reports
				GROUP BY extract('DOW' from data_insert)
				) qr1
			ORDER BY total DESC";

			$arrayTemp = $this->conn->retornaArray($strsql);

			return $arrayTemp;
		}

	}
