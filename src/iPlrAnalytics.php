<?php
	
	interface iPlrAnalytics{

		public function getTotalDeResultados();
		public function getTotalDeResultadosValidados();
		public function getTotalDeReports();
		public function getTotalDeReportsRespondidos();
		public function getPercentualDeValidados();
		public function getPercentualDeReportados();
		public function getPercentualDeReportsRespondidos();
		public function getReportsPorAcesso();
		public function getReportsPorHora();
		public function getReportsPorMatricula();
		public function getReportsPorDiaDaSemana();

	}
