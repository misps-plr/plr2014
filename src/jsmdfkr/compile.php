<?php

	chdir('../../assets/js');

	if(is_dir(getcwd())){
		$diretorio = dir(getcwd());

		$file = null;

		while($arquivo = $diretorio->read()){
			if(is_file($arquivo) and $arquivo != 'all.php'){
				$file .= file_get_contents($arquivo, true);
			}
		}


		header('Content-Type: application/javascript');
		echo $file;

		$diretorio->close();
	}
	else{
		echo 'A directory não existe.';
	}