<?php
	interface IUsuario
	{

		public function getMatricula();
		public function getNome();
		public function getCentroDeCusto();
		public function getCargo();
		public function getCelula();
		public function getGerente();

	    public function metaDados();

	}