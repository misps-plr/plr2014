<?php
	
	require 'ConexaoBancoMisPg.php';
	require 'IUsuario.php';
	/**
	* Classe que busca os resultados do agente, recebe como argumento a matricula
	*/
	class Usuario implements IUsuario{
		
		private $matricula;
		private $nome;
		private $centroDeCusto;
		private $cargo;
		private $celula;
		private $gerente;
		
		function __construct($matricula){
			$this->conn = new ConexaoBancoMisPg();
			$this->matricula = $matricula;
			
			$metaDados = $this->metaDados();

			$this->nome = strtoupper(odbc_result($metaDados, 'full_name'));
			$this->centroDeCusto = strtoupper(odbc_result($metaDados, 'cost_center'));
			$this->cargo = strtoupper(odbc_result($metaDados, 'job_position'));
			$this->celula= strtoupper(odbc_result($metaDados, 'work_cell'));
			$this->gerente= strtoupper(odbc_result($metaDados, 'manager'));
		}

		public function getMatricula(){
			return $this->matricula;
		}
		
		public function getNome(){
			return $this->nome;
		}
		
		public function getCentroDeCusto(){
			return $this->centroDeCusto;
		}
		
		public function getCargo(){
			return $this->cargo;
		}
		
		public function getCelula(){
			return $this->celula;
		}
		
		public function getGerente(){
			return $this->gerente;
		}

	    public function metaDados(){
	    	return $this->conn->executaRetorno("SELECT * FROM proc_meta_data('".$this->matricula."')");
	    }

	}
