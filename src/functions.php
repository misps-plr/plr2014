<?php



	class Main{

		function __construct(){
			$this->base_path = dirname(__FILE__);
		}

		function fatal_error($msg){
			header('Content-Type: text/html; charset=utf-8');
			echo '<h1>Erro Fatal</h1>';
			echo $msg;
			exit;
		}

		function partial($file){
			if(!file_exists('./views/'.$file.'.php')){
				$this->fatal_error('Não foi possível localizar o partial: '.$file);
				exit;
			}

			require './views/'.$file.'.php';
		}

		

	}