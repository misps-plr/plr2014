<?php

	require 'Usuario.php';

	class Plr{

		function __construct($matricula){
			$this->usuario = new Usuario($matricula);
			$this->conn = $this->usuario->conn; 
		}

		# retrieve an property
		function get($option){
			return $this->$option;
		}		

		# set an property
		function set($option, $value){
			$this->$option = $value;
		}

		# modafockamente _\,,/
		function alomafocka($value){
			return (is_numeric($value)) ? $value : "'{$value}'" ;
		}

		# recebe um vetor para iterar ele
		function itera($array){
			$itr = null;

			foreach($array as $value){
				$itr .= $this->alomafocka($value).',';
			}

			return substr($itr, 0, -1);
		}

		#
		function tem_parametro($param){
			if(empty($param)){
				echo "Você não forneceu o parâmetro um ou mais parametros necessários para acessar este recurso.";
				exit();
			}
		}

		#
		function exibe_datahora($string){
			return date('d/m/Y H:i', strtotime($string));
		}

		#
		function saudacao(){
			if(date('H') > 0 && date('H') < 13){
				$saudacao = 'Bom dia';
			}
			else if(date('H') > 12 && date('H') < 18){
				$saudacao = 'Boa tarde';
			}
			else if(date('H') > 17 && date('H') <= 23){
				$saudacao = 'Boa noite';
			}
			else{
				$saudacao = 'Unknow';
			}

			return $saudacao;
		}

		#
		function primeiroNome($nome){
			$part = explode(' ', $nome);

			return $part[0];
		}

		#
		function assinatura(){
			$assinatura = "\r\n\r\n\r\n\r\n";
			$assinatura .= "PLR 2014 \r\n";
			$assinatura .= "MIS Atendimento \r\n";
			$assinatura .= "Porto Seguro \r\n";

			return $assinatura;
		}

		#
		function adm_level(){
			global $sso;

			$matricula = $sso->get_user_data('uid');
			$resultset = $this->conn->executaRetorno("SELECT nivel FROM plr_2014.tbl_administradores WHERE matricula={$matricula}");

			return odbc_result($resultset, 'nivel');
		}

		#
		function is_adm(){
			global $sso;

			$matricula = $sso->get_user_data('uid');
			$resultset = $this->conn->executaRetorno("SELECT nivel FROM plr_2014.tbl_administradores WHERE matricula={$matricula}");

			return (empty($resultset)) ? false : true ;
		}

		#
		function dados_report($report_id){
			$rs = $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_report_metadata('{$report_id}')");

			if(empty($rs)){
				return false;
			}
			
			return odbc_fetch_array($rs);
		}

		#
		function muda_status_report($report, $analyst, $status){
			$rs = $this->conn->executaRetorno("UPDATE plr_2014.tbl_reports SET status={$status}, cfmis={$analyst} WHERE id={$report}");

			if(empty($rs)){
				return false;
			}

			return true;
		}

		# retorna todos os novos reports
		function reports_novos(){
			$strsql = "SELECT * FROM plr_2014.proc_reports_novos() WHERE send_datetime>'2015-01-19 18:00:00' ORDER by send_datetime DESC";

			return $this->conn->retornaArray($strsql);
		}

		# retorna todos os reports em atendimento
		function reports_atendimento($matricula){

			$strsql  = "SELECT * FROM plr_2014.proc_reports_atendimento() WHERE send_datetime>'2015-01-19 18:00:00' ";
			$strsql .= ($this->adm_level() == 2 or $this->adm_level() == 3) ? "" : " AND analyst_uid='{$matricula}' " ;
			$strsql .= "ORDER by send_datetime DESC";

			return $this->conn->retornaArray($strsql);
		}

		# retorna todos os reports resolvidos
		function reports_resolvidos($matricula){

			$strsql  = "SELECT * FROM plr_2014.proc_reports_solucionados() WHERE send_datetime>'2015-01-19 18:00:00' ";
			$strsql .= ($this->adm_level() == 2 or $this->adm_level() == 3) ? "" : " AND analyst_uid='{$matricula}' " ;
			$strsql .= "ORDER by send_datetime DESC";

			return $this->conn->retornaArray($strsql);
		}

		function reportar(){
			$usuario    = $_POST['uid'];
			$comentario = str_replace ( "'" ,"", $_POST['comment']);
			
			# Monta vetor com resultados a serem reportados!
			$resultados = array();

			foreach($_POST as $key => $value){
				if($value == 'resultado'){
					$resultados[] .= $key;
				}
			}

			$resultados = implode(',', $resultados);

			$strsql = "SELECT * FROM plr_2014.proc_reporta(ARRAY[$resultados], '$comentario', '$usuario')";
			$rs = $this->conn->executaRetorno($strsql);
			$sucesso = $rs == 0 ?  0 : $sucesso + 1;


			if($sucesso > 0){
				return true;
			}

			return false;
		}

		function responder($report, $comment, $analyst){
			$rs = $this->conn->executaRetorno("SELECT plr_2014.proc_responde_report($report, '$comment', $analyst)");
			echo (empty($rs)) ? 'error;Não foi possível enviar a mensagem para o usuário;' : 'success;A mensagem foi enviada, por favor aguarde ... !;' ;

			/*if(empty($_POST['comment'])){
				return false;
			}
			else{
				$strsql = "SELECT plr_2014.proc_responde_report($report, '$comment', $analyst)";
				$rs = $this->conn->executaRetorno($strsql);
				$sucesso = $rs == 0 ?  0 : $sucesso + 1;

				if($sucesso > 0){
					return true;
				}
				return false;	
			}*/
		}


		#
		function retornaReportados($uid, $type){
			$strsql = "SELECT * FROM ";
			$strsql .= ($type == 2) ? "plr_2014.proc_consulta_report() " : "plr_2014.proc_consulta_report('$uid')" ;
			$strsql .= "WHERE send_datetime>'2015-01-19 18:00:00' ORDER by send_datetime DESC";

			return $this->conn->retornaArray($strsql);
		}

		#
		function resultadosTabela($report_id){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_consulta_comparacao('{$report_id}')");
		}

		

		# Retorna resultados que uma pessoa reportou, filtrando por data a partir de 20/01/2015
		function reportados_por_pessoa($uid){
			$strsql = "SELECT * FROM plr_2014.proc_consulta_report('$uid') WHERE send_datetime>'2015-01-19 18:00:00' ORDER by send_datetime DESC";

			return $this->conn->retornaArray($strsql);
		}

		function validar(){
			$sucesso = 0;

			foreach($_POST as $key => $value) {
			  $strsql = "UPDATE plr_2014.tbl_extrato_test SET validado = $value, reportado = false WHERE id = $key";

			  $rs = $this->conn->executaRetorno($strsql);
			  $sucesso = $rs == 0 ?  0 : $sucesso + 1;
			}

			if ($sucesso == count($_POST)) {
				return true;
			}

			return false;
		}


		## DEPRECAR

		# formata o retorno da data oriunda do pgsql
		function donbeatybutwork($string){
			return $this->exibe_datahora($string);
		}

		#
		function trataValores($results, $key , $id, $results_ant){

			$simbol = '';

			if ($results == '' or $results == '-1' or $results == '-1%-1' or $results == '-1-1') {
				$results = '-';
			}

			if($key == 'mes' and $results == '0_0'){
				$results = 'Total';
			}

			if(sizeof(split("_",$results)) == 2 and split("_",$results)[1] <> '-1' and $key <> 'mes'){
				$simbol = "<img class='' height='15px' src='assets/img/".split("_",$results)[1].".svg'/>";
				$results = str_replace('_', '', split("_",$results)[0]);

			}elseif($key == 'mes'){
				if(str_replace('_', '', split("_",$results)[1]) <> '')
					$results = str_replace('_', '', split("_",$results)[1]);

			}elseif (sizeof(split("_",$results)) == 2 and split("_",$results)[1] == '-1') {
				$results = str_replace('_', '', split("_",$results)[0]);

			}

			return array($results, $simbol);
		}

		# from Resultados Report
		function insane($report_id){
			#
			$result_set = $this->resultadosTabela($report_id);

			if(empty($result_set)){
				return false;
				exit;
			}

			#
			$html .= '<div class="results-table" id="results-table" style="max-height: 400px;overflow-y: auto;">';
			$html .= "<table class='table' >";

			while($array = odbc_fetch_array($result_set)){

				$id = current($array);
				$nivel = next($array);
				
				$html .= "<tr id='".$id."' class = 'row results-".$nivel."'>";
					
				next($array);

				$results_ant = '';

				while (key($array)) {
					if (current($array) <> '-' and key($array) <> 'validado' and key($array) <> 'reportado'){
						$results = $this->trataValores(current($array),key($array),$id,$results_ant);
					
						$html .= '<td class="row-content">';
						$html .= '<span class="fl">'.strtoupper($results[0]).'</span>';
						$html .= '<span class="fl rank-position">'.$results[1].'</span>';
						$html .= '</td>';

						$results_ant = current($array);
					}
					
					next($array);
				}

				$html .= "</tr>";
			}
			
			$html .= '</table>';
			$html .= '</div>';

			return $html;
		}

		


	}