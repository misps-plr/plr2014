<?php
	
	require 'Plr.php';

	/**
	* Classe que busca os resultados do agente, recebe como argumento a matricula
	*/
	class ResultadosReport extends Plr{
		
		function __construct($id_report){
			$this->usuario = new Usuario($matricula);
			$this->conn = $this->usuario->conn;
			$this->id_report = $id_report;
		}

		function trataValores($results,$key,$id,$results_ant){

			$simbol = '';

			if ($results == '' or $results == '-1' or $results == '-1%-1' or $results == '-1-1') {
				$results = '-';
			}

			if($key == 'mes' and $results == '0_0'){
				$results = 'Total';
			}

			if(sizeof(split("_",$results)) == 2 and split("_",$results)[1] <> '-1' and $key <> 'mes'){
				$simbol = "<img class='' height='15px' src='assets/img/".split("_",$results)[1].".svg'/>";
				$results = str_replace('_', '', split("_",$results)[0]);
			}elseif($key == 'mes'){
				if(str_replace('_', '', split("_",$results)[1]) <> '')
					$results = str_replace('_', '', split("_",$results)[1]);
			}elseif (sizeof(split("_",$results)) == 2 and split("_",$results)[1] == '-1') {
				$results = str_replace('_', '', split("_",$results)[0]);
			}

			
			return array($results,$simbol);
		
		}

		function resultadosMeta(){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_report_metadata('{$this->id_report}')");
		}

		function resultadosTabela(){
			return $this->conn->executaRetorno("SELECT * FROM plr_2014.proc_consulta_comparacao('{$this->id_report}')");
		}

		




		

		function retornaResultado(){
			global $sso;
			
			$sso->is_loggedin(); # resolve bug que permite acessar a tela de resultados sem estar logado
			
			$uid = $sso->get_user_data('uid');

			$meta_result  = odbc_fetch_array($this->resultadosMeta());
			$table_result = $this->resultadosTabela();

			#
			$html  = '<div class="report-data">';
			$html .= '	<h1 class="uppercase">'.$meta_result['agent_name'].' ('.$meta_result['agent_uid'].')</h1>';

			$html .= '	<div class="scroll" style="height: 400px">';
			$html .= '		<div class="request">';
			$html .= '			<h2>Solicitação</h2>';
			$html .= '			<div class="description">'.nl2br($meta_result['comment']).'</div>';
			$html .= '			<ul class="meta">';
			$html .= '				<li class="fl"><span class="bolder">Aberto em:</span><span>'.$this->exibe_datahora($meta_result['send_datetime']).'</span>';
			$html .= '				<li class="fl"><span class="bolder">Solicitante:</span><span class="uppercase">'.$meta_result['sender_name'].'</span></li>';
			$html .= '			</ul>';
			$html .= '			<div class="clear"></div>';
			$html .= '		</div><!-- /fim request-->';

			#
			if($meta_result['status'] != 'pendente'){
				$html .= '	<div class="separator"></div>';
				$html .= '	<div class="solution">';
				$html .= '		<h2>Solução</h2>';
				$html .= '		<div class="description">'.nl2br($meta_result['commentmis']).'</div>';
				$html .= '		<ul class="meta">';
				$html .= '			<li class="fl"><span class="bolder">Status:</span><span class="capitalized">'.$meta_result['status'].'</span>';
				$html .= '			<li class="fl"><span class="bolder">Data:</span><span>'.$this->exibe_datahora($meta_result['closed_when']).'</span>';
				$html .= '			<li class="fl"><span class="bolder">Analista:</span><span class="uppercase">'.$meta_result['mis_analyst'].'</span></li>';
				$html .= '		</ul>';
				$html .= '		<div class="clear"></div>';
				$html .= '	</div>';
			}

			#
			if($sso->get_user_data('type') != 2){
				$html .= '<div class="separator"></div>';
				$html .= '<div class="results-table" id="results-table" style="max-height: 400px;overflow-y: auto;">';
				$html .= "<table class='table' >";

				while($array = odbc_fetch_array($table_result)){

					$id = current($array);
					$nivel = next($array);
					
					$html .= "<tr id='".$id."' class = 'row results-".$nivel."'>";
						
					next($array);

					$results_ant = '';

					while (key($array)) {
						
						if (current($array) <> '-' and key($array) <> 'validado' and key($array) <> 'reportado'){
							$results = $this->trataValores(current($array),key($array),$id,$results_ant);
						
							$html .= '<td class="row-content">';
							$html .= '<span class="fl">'.strtoupper($results[0]).'</span>';
							$html .= '<span class="fl rank-position">'.$results[1].'</span>';
							$html .= '</td>';

							$results_ant = current($array);
						}
						
						next($array);
					}

					$html .= "</tr>";

				}
				
				$html .= '</table>';
				$html .= '</div>';

			}
			else{ ////// ANALISTAS DO MIS
				$html .= '<div class="report-comments" style="max-height:400px;overflow:auto;">';
				$html .= '<table class="results-table scroll" >';

				while($array = odbc_fetch_array($table_result)){

					$id = current($array);
					$nivel = next($array);
					
					$html .= "<tr id='".$id."' class = 'row results-".$nivel."'>";
						
					next($array);

					$results_ant = '';

					while (key($array)) {
						
						if (current($array) <> '-' and key($array) <> 'validado' and key($array) <> 'reportado'){
							$results = $this->trataValores(current($array),key($array),$id,$results_ant);
						
							$html .= '<td class="row-content">';
							$html .= '<span class="fl">'.strtoupper($results[0]).'</span>';
							$html .= '<span class="fl rank-position">'.$results[1].'</span>';
							$html .= '</td>';

							$results_ant = current($array);
						}
						
						next($array);
					}

					$html .= "</tr>";

				}
				
				$html .= '</table>';
				$html .= '</div>';
				$html .= '<div class="separator"></div>';
				$html .= '<div class="report-comments" style="max-height:250px;overflow:auto;">';
				$html .= '	<h2>Resposta</h2>';
				$html .= "	<form method='post'>";
				$html .= '		<textarea class="comment-area" id="comment" name="comment" placeholder="" autofocus required></textarea>';
				$html .= '		<div class="actions fr" id="reported-actions" >';
				$html .= '			<input type="button" onclick="acaoRespostaReport()" id="RESPOSTA_REPORT" value="Enviar" class="button fr success">';
				$html .= '		</div>';
				$html .= '		<input type="hidden" id="analista_mis" name="mis" value="'.$uid.'">'; 
				$html .= '		<input type="hidden" name="report_id" value="">'; 
				$html .= '	</form>';
				$html .= '</div>';
			}

			$html .= '	</div><!-- /fim results-table c/ scroll -->';
			$html .= '</div><!-- /fim dados do report -->';

			return $html;
		}
	}