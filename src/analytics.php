<?php
	require 'PlrAnalytics.php';
	$analytics = New PlrAnalytics();
?>

<html>
<head>
	<meta http-equiv="refresh" content="5" >
	<title>PLR 2014 :: Analytics</title>
</head>
<body>
	<div class="analytics">

		<h2>Total de acessos:</h2>
		<h3><?php echo $analytics->getTotalDeAcessos(); ?></h3>
		
		<!-- Percentual de resultados validados
		public function getPercentualDeValidados();
			public function getTotalDeResultadosValidados();
			public function getTotalDeResultados();-->
		<h2>Resultados validados</h2>

		<svg width="800" height="45">
			 <svg x="<?php echo $analytics->getPercentualDeValidados() - 1; ?>%">
		    	<text x="0" y="15" height="100" width="100" style="fill: #0A2D56"><?php echo round($analytics->getPercentualDeValidados()); ?>%</text>
		  	</svg>
		  <rect width="100%" height="15" y="16" style="fill:#205EA8" />
		  <rect width="<?php echo $analytics->getPercentualDeValidados(); ?>%" height="15" y="16" style="fill:#0A2D56" />
		</svg>

		<!-- Percentual de resultados reportados
		public function getPercentualDeReportados();
			public function getTotalDeReports();
			public function getTotalDeReportsRespondidos();-->
		<h2>Resultados reportados</h2>
		
		<svg width="800" height="45">
			 <svg x="<?php echo $analytics->getPercentualDeReportados() - 1; ?>%">
		    	<text x="0" y="15" height="100" width="100" style="fill: #0A2D56"><?php echo round($analytics->getPercentualDeReportados()); ?>%</text>
		  	</svg>
		  <rect width="100%" height="15" y="16" style="fill:#205EA8" />
		  <rect width="<?php echo $analytics->getPercentualDeReportados(); ?>%" height="15" y="16" style="fill:#0A2D56" />
		</svg>


		<!-- Percentual de reports respondidos
		public function getPercentualDeReportsRespondidos();
		-->
		<h2>Resultados reports respondidos</h2>
		
		<svg width="800" height="45">
			 <svg x="<?php echo $analytics->getPercentualDeReportsRespondidos() - 1; ?>%">
		    	<text x="0" y="15" height="100" width="100" style="fill: #0A2D56"><?php echo round($analytics->getPercentualDeReportsRespondidos()); ?>%</text>
		  	</svg>
		  <rect width="100%" height="15" y="16" style="fill:#205EA8" />
		  <rect width="<?php echo $analytics->getPercentualDeReportsRespondidos(); ?>%" height="15" y="16" style="fill:#0A2D56" />
		</svg>

		<!-- Total de acesso desde a publicacoa 
		public function getTotalDeAcessos()
		-->
		<!-- Outros indicadores 
			public function getReportsPorMatricula();
			public function getReportsPorAcesso();
			public function getReportsPorHora();
			public function getReportsPorDiaDaSemana();
			-->
	</div>

</body>
</html>

